package QueryFlink;

import Utils.CalendarUtils;
import Utils.RankQuery;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;

import java.util.Date;

public class PrimaQuery {

    /*
        questa funzione esegue la prima query. Prende in ingresso uno stream Tuple3<timestamp,idUtente1,idUtente2> e il producer
        su cui effettuare il sink.
     */
    public static void executeQuery1(DataStream<Tuple3<Long, Long, Long>> stream, FlinkKafkaProducer010<String> producer) {
        KeyedStream<Tuple3<Long, Integer, Long>, Tuple> streamFriendshipKeyed = stream
                //mappo la tupla3 in una nuova Tuple3<timestamp,hour_of_day,1>
                //il timestamp lo porto avanti per poter stampare nella classifica l'ultimo timestamp della statistica
                .map(new MapFunction<Tuple3<Long, Long, Long>, Tuple3<Long, Integer, Long>>() {
                    @Override
                    public Tuple3<Long, Integer, Long> map(Tuple3<Long, Long, Long> longLongLongTuple3) throws Exception {
                        return new Tuple3<>(longLongLongTuple3.f0, CalendarUtils.getHourOfDay(new Date(longLongLongTuple3.f0)), 1L);
                    }
                })

                //raggruppo per ora giornaliera
                .keyBy(1);

        SingleOutputStreamOperator<Tuple3<Long, Integer, Long>> rank24HFriendship = streamFriendshipKeyed
                .window(TumblingEventTimeWindows.of(Time.days(1)))
                .allowedLateness(Time.seconds(2))
                //sommo tutti i valori con la stessa ora giornaliera all'interno della prima finestra.
                //nel calcolo della reduce mi salvo anche il timestamp più recente, per poter soddisfare il requisito della query scritto nella traccia del progetto (ovvero timestamp di inizio statistica)
                .reduce(new ReduceFunction<Tuple3<Long, Integer, Long>>() {
                    @Override
                    public Tuple3<Long, Integer, Long> reduce(Tuple3<Long, Integer, Long> longIntegerLongTuple3, Tuple3<Long, Integer, Long> t1) throws Exception {
                        return new Tuple3<>(longIntegerLongTuple3.f0 > t1.f0 ? longIntegerLongTuple3.f0 : t1.f0, longIntegerLongTuple3.f1, longIntegerLongTuple3.f2 + t1.f2);
                    }
                });

        RankQuery.producerRankQuery1(rank24HFriendship, producer, "Classifica 24h", "ora", "value");


        SingleOutputStreamOperator<Tuple3<Long, Integer, Long>> rank7DaysFriendship = rank24HFriendship
                .keyBy(1)
                .window(TumblingEventTimeWindows.of(Time.days(7), Time.days(4)))
                .allowedLateness(Time.seconds(2))
                .reduce(new ReduceFunction<Tuple3<Long, Integer, Long>>() {
                    @Override
                    public Tuple3<Long, Integer, Long> reduce(Tuple3<Long, Integer, Long> longIntegerLongTuple3, Tuple3<Long, Integer, Long> t1) throws Exception {
                        return new Tuple3<>(longIntegerLongTuple3.f0 > t1.f0 ? longIntegerLongTuple3.f0 : t1.f0, longIntegerLongTuple3.f1, longIntegerLongTuple3.f2 + t1.f2);
                    }
                });
        RankQuery.producerRankQuery1(rank7DaysFriendship, producer, "Classifica 7 giorni", "ora", "value");


        SingleOutputStreamOperator<Tuple3<Long, Integer, Long>> rankHistoryFriendship =
                streamFriendshipKeyed
                        .reduce(new ReduceFunction<Tuple3<Long, Integer, Long>>() {
                            @Override
                            public Tuple3<Long, Integer, Long> reduce(Tuple3<Long, Integer, Long> longIntegerLongTuple3, Tuple3<Long, Integer, Long> t1) throws Exception {
                                return new Tuple3<>(longIntegerLongTuple3.f0 > t1.f0 ? longIntegerLongTuple3.f0 : t1.f0, longIntegerLongTuple3.f1, longIntegerLongTuple3.f2 + t1.f2);

                            }
                        })
                        .keyBy(1)
                        .timeWindow(Time.days(2))
                        .allowedLateness(Time.seconds(2))
                        .maxBy(2);
        RankQuery.producerRankQuery1(rankHistoryFriendship, producer, "Classifica storica", "ora", "value");


    }
}
