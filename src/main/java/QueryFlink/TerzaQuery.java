package QueryFlink;

import Utils.RankQuery;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;


public class TerzaQuery {


    /*
        questa funzione esegue la terza query. Prende in ingresso tre stream Tuple3<timestamp,post_id> (uno per il dataset friend,
        uno per i posts e uno per i comments), e il producer
        su cui effettuare il sink.
    */

    public static void executeThirdQuery(DataStream<Tuple3<Long, Long, Long>> friendsDataStream, DataStream<Tuple2<Long, Long>> postsDataStream, DataStream<Tuple2<Long, Long>> commentsDataStream, FlinkKafkaProducer010<String> producer) {
        //prendo il dataset friend e faccio una flatmap (per conteggiare come amicizie sia quella dell'id 1, sia quella dell'id 2)
        DataStream<Tuple3<Long, Long, Long>> friendsDataStreamMapped =
                friendsDataStream
                        .flatMap(new FlatMapFunction<Tuple3<Long, Long, Long>, Tuple3<Long, Long, Long>>() {
                            @Override
                            public void flatMap(Tuple3<Long, Long, Long> longLongLongTuple3, Collector<Tuple3<Long, Long, Long>> collector) throws Exception {
                                Tuple3<Long, Long, Long> tuple3 = new Tuple3<>();
                                tuple3.f0 = longLongLongTuple3.f0;
                                tuple3.f1 = longLongLongTuple3.f1;
                                tuple3.f2 = 1L;
                                collector.collect(tuple3);

                                tuple3 = new Tuple3<>();
                                tuple3.f0 = longLongLongTuple3.f0;
                                tuple3.f1 = longLongLongTuple3.f2;
                                tuple3.f2 = 1L;
                                collector.collect(tuple3);
                            }
                        });

        DataStream<Tuple3<Long, Long, Long>> postsDataStreamMapped =
                postsDataStream
                        .map(new MapFunction<Tuple2<Long, Long>, Tuple3<Long, Long, Long>>() {
                            @Override
                            public Tuple3<Long, Long, Long> map(Tuple2<Long, Long> longLongTuple2) throws Exception {
                                return new Tuple3<>(longLongTuple2.f0, longLongTuple2.f1, 1L);
                            }
                        });

        DataStream<Tuple3<Long, Long, Long>> commentsDataStreamMapped =
                commentsDataStream
                        .map(new MapFunction<Tuple2<Long, Long>, Tuple3<Long, Long, Long>>() {
                            @Override
                            public Tuple3<Long, Long, Long> map(Tuple2<Long, Long> longLongTuple2) throws Exception {
                                return new Tuple3<>(longLongTuple2.f0, longLongTuple2.f1, 1L);
                            }
                        });


        //unisco gli stream con una union e ci rialloco il timestamp
        SingleOutputStreamOperator<Tuple3<Long, Long, Long>> rank1Hour = friendsDataStreamMapped.union(postsDataStreamMapped, commentsDataStreamMapped)
                .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Tuple3<Long, Long, Long>>() {
                    @Override
                    public long extractAscendingTimestamp(Tuple3<Long, Long, Long> longLongLongTuple3) {
                        return longLongLongTuple3.f0;
                    }
                })
                //li raggruppo per id dell'utetne
                .keyBy(1)
                .timeWindow(Time.hours(1))
                //faccio la reduce sul contatore
                .reduce(new ReduceFunction<Tuple3<Long, Long, Long>>() {
                    @Override
                    public Tuple3<Long, Long, Long> reduce(Tuple3<Long, Long, Long> longLongLongTuple3, Tuple3<Long, Long, Long> t1) throws Exception {
                        return new Tuple3<>(longLongLongTuple3.f0 > t1.f0 ? longLongLongTuple3.f0 : t1.f0, longLongLongTuple3.f1, longLongLongTuple3.f2 + t1.f2);
                    }
                });


        RankQuery.producerRank(rank1Hour, producer, "classifica 1 ora", "user_id", "value");

        //sfrutto lo stream precedente (mandato in output su kafka) e lo riutilizzo per farci la reduce su una finestra temporale di 24 ore
        SingleOutputStreamOperator<Tuple3<Long, Long, Long>> rank24Hours = RankQuery.reduceRankFromOldRank(rank1Hour, 1);
        RankQuery.producerRank(rank24Hours, producer, "classifica 24 ore", "user_id", "value");


        SingleOutputStreamOperator<Tuple3<Long, Long, Long>> rank7Days = rank24Hours
                .keyBy(1)
                .window(TumblingEventTimeWindows.of(Time.days(7), Time.days(4)))
                .reduce(new ReduceFunction<Tuple3<Long, Long, Long>>() {
                    @Override
                    public Tuple3<Long, Long, Long> reduce(Tuple3<Long, Long, Long> longLongLongTuple3, Tuple3<Long, Long, Long> t1) throws Exception {
                        return new Tuple3<>(longLongLongTuple3.f0 > t1.f0 ? longLongLongTuple3.f0 : t1.f0, longLongLongTuple3.f1, longLongLongTuple3.f2 + t1.f2);

                    }
                });

        RankQuery.producerRank(rank7Days, producer, "classifica 7 giorni", "user_id", "value");


    }
}
