package QueryFlink;

import Utils.RankQuery;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;

public class SecondaQuery {

    /*
        questa funzione esegue la seconda query. Prende in ingresso uno stream Tuple3<timestamp,post_id> e il producer
        su cui effettuare il sink.
    */
    public static void executeSecondQuery(DataStream<Tuple2<Long, Long>> streamQuery2, FlinkKafkaProducer010<String> producer) {
        KeyedStream<Tuple3<Long, Long, Long>, Tuple> keyedStream = streamQuery2
                //mappo lo stream in input in un nuovo stream Tuple3<timestamp,id_post,1>
                .map(new MapFunction<Tuple2<Long, Long>, Tuple3<Long, Long, Long>>() {
                    @Override
                    public Tuple3<Long, Long, Long> map(Tuple2<Long, Long> longLongTuple2) throws Exception {
                        return new Tuple3<>(longLongTuple2.f0, longLongTuple2.f1, 1L);
                    }
                })
                .keyBy(1);

        SingleOutputStreamOperator<Tuple3<Long, Long, Long>> rank1hour = keyedStream
                .window(TumblingEventTimeWindows.of(Time.hours(1)))
                .allowedLateness(Time.seconds(10))
                //faccio la reduce (quindi conto) di tutte le tuple con stesso post_id, e inserisco di volta in volta il timestamp più recente
                .reduce(new ReduceFunction<Tuple3<Long, Long, Long>>() {
                    @Override
                    public Tuple3<Long, Long, Long> reduce(Tuple3<Long, Long, Long> longLongLongTuple3, Tuple3<Long, Long, Long> t1) throws Exception {

                        return new Tuple3<>(longLongLongTuple3.f0 > t1.f0 ? longLongLongTuple3.f0 : t1.f0, longLongLongTuple3.f1, longLongLongTuple3.f2 + t1.f2);
                    }
                });


        RankQuery.producerRank(rank1hour, producer, "classifica 1 ora", "post_id", "num_comments");

        //riutilizzo lo stream di prima per generare la classifica da 24 ore
        SingleOutputStreamOperator<Tuple3<Long, Long, Long>> rank24Hours = RankQuery.reduceRankFromOldRank(rank1hour, 1);

        RankQuery.producerRank(rank24Hours, producer, "classifica 24 ore", "post_id", "num_comments");

        //SingleOutputStreamOperator<Tuple2<Long, Long>> rank1Week= RankQuery.reduceRankFromOldRank(rank24Hours,7);

        SingleOutputStreamOperator<Tuple3<Long, Long, Long>> rank1Week = rank24Hours
                .keyBy(1)
                .window(TumblingEventTimeWindows.of(Time.days(7), Time.days(4)))
                .reduce(new ReduceFunction<Tuple3<Long, Long, Long>>() {
                    @Override
                    public Tuple3<Long, Long, Long> reduce(Tuple3<Long, Long, Long> longLongLongTuple3, Tuple3<Long, Long, Long> t1) throws Exception {
                        return new Tuple3<>(longLongLongTuple3.f0 > t1.f0 ? longLongLongTuple3.f0 : t1.f0, longLongLongTuple3.f1, longLongLongTuple3.f2 + t1.f2);
                    }
                });
        RankQuery.producerRank(rank1Week, producer, "classifica 7 giorni", "post_id", "num_comments");


    }
}
