package Utils;

import org.apache.flink.api.java.tuple.Tuple2;

import java.util.Comparator;

public class ComparatorRank implements Comparator<Tuple2<Long, Long>> {
    @Override
    public int compare(Tuple2<Long, Long> o1, Tuple2<Long, Long> o2) {
        if (o1.f1 < o2.f1)
            return 1;
        else if (o1.f1 > o2.f1)
            return -1;
        else
            return 0;
    }
}
