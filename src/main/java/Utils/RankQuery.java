package Utils;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Date;

public class RankQuery {

    public static void producerRank(SingleOutputStreamOperator<Tuple3<Long, Long, Long>> stream, FlinkKafkaProducer010<String> producer, String typeRank, String id, String value) {
        stream
                //raggruppo tutte le tuple con post_id diverso, per generare la classifica
                .timeWindowAll(Time.seconds(2))
                .apply(new AllWindowFunction<Tuple3<Long, Long, Long>, ArrayList<Tuple3<Long, Long, Long>>, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow timeWindow, Iterable<Tuple3<Long, Long, Long>> iterable, Collector<ArrayList<Tuple3<Long, Long, Long>>> collector) throws Exception {
                        //utilizzo l'array list per generare la classifica
                        OrderedArrayList.ArrayListOrderedDecreasingIndex2 arrayListOrderedDecreasingIndex2 = new OrderedArrayList.ArrayListOrderedDecreasingIndex2();
                        iterable.forEach(arrayListOrderedDecreasingIndex2::insertOrdered);

                        collector.collect(arrayListOrderedDecreasingIndex2);

                    }
                })
                .map(new MapFunction<ArrayList<Tuple3<Long, Long, Long>>, String>() {
                    @Override
                    public String map(ArrayList<Tuple3<Long, Long, Long>> tuple3s) throws Exception {
                        //prendo l'ultimo timestamp per inserirlo nella clasisifca
                        OrderedArrayList.ArrayListLongOrderedDecreasing arrayListLongOrderedDecreasing = new OrderedArrayList.ArrayListLongOrderedDecreasing();
                        tuple3s.forEach(longLongLongTuple3 -> arrayListLongOrderedDecreasing.add(longLongLongTuple3.f0));
                        Long lastTimestamp = arrayListLongOrderedDecreasing.get(0);

                        String output;
                        output = (typeRank + "\n");
                        output += "ts:" + CalendarUtils.getTimestampFormatted(new Date(lastTimestamp)) + "\n";
                        for (int i = 0; i < tuple3s.size() && i < 10; i++) {
                            output += id + ": " + tuple3s.get(i).f1 + " " + value + ": " + tuple3s.get(i).f2 + "\n";

                        }

                        return output;
                    }
                }).addSink(producer);
    }

    public static SingleOutputStreamOperator<Tuple3<Long, Long, Long>> reduceRankFromOldRank(SingleOutputStreamOperator<Tuple3<Long, Long, Long>> stream, int days) {
        //prendo lo stream in input e faccio ancora una volta la reduce sul contatore.
        //in questo modo evito di fare somme già fatte, perchè prendo lo stream già calcolato e continuo a farci sopra la reduce
        return stream
                .keyBy(1)
                .window(TumblingEventTimeWindows.of(Time.days(days)))
                .reduce(new ReduceFunction<Tuple3<Long, Long, Long>>() {
                    @Override
                    public Tuple3<Long, Long, Long> reduce(Tuple3<Long, Long, Long> longLongLongTuple3, Tuple3<Long, Long, Long> t1) throws Exception {
                        return new Tuple3<>(longLongLongTuple3.f0 > t1.f0 ? longLongLongTuple3.f0 : t1.f0, longLongLongTuple3.f1, longLongLongTuple3.f2 + t1.f2);

                    }
                });
    }

    public static void producerRankQuery1(SingleOutputStreamOperator<Tuple3<Long, Integer, Long>> stream, FlinkKafkaProducer010<String> producer, String typeRank, String id, String value) {
        stream
                //aspetto che vengano aggregati tutti i valori di tutte le chiavi, per poter generare la classifca
                .timeWindowAll(Time.seconds(2))
                .apply(new AllWindowFunction<Tuple3<Long, Integer, Long>, Tuple2<Long, ArrayList<Long>>, TimeWindow>() {
                    @Override
                    public void apply(TimeWindow timeWindow, Iterable<Tuple3<Long, Integer, Long>> iterable, Collector<Tuple2<Long, ArrayList<Long>>> collector) throws Exception {
                        //calcolo timestamp più recente
                        OrderedArrayList.ArrayListLongOrderedDecreasing arrayListLongOrderedDecreasing = new OrderedArrayList.ArrayListLongOrderedDecreasing();
                        iterable.forEach(longIntegerLongTuple3 -> arrayListLongOrderedDecreasing.add(longIntegerLongTuple3.f0));
                        Long timestamp = arrayListLongOrderedDecreasing.get(0);

                        //calcolo rank
                        ArrayList<Long> rank = new ArrayList<>();
                        for (int i = 0; i < 24; i++) {
                            rank.add(i, 0L);
                        }
                        iterable.forEach(integerLongTuple2 -> {
                            int index = (int) (long) integerLongTuple2.f1;
                            Long oldValue = rank.get(index);
                            rank.set(index, oldValue + integerLongTuple2.f2);
                        });
                        collector.collect(new Tuple2<>(timestamp, rank));
                    }
                })
                .map(new MapFunction<Tuple2<Long, ArrayList<Long>>, String>() {
                    @Override
                    public String map(Tuple2<Long, ArrayList<Long>> longArrayListTuple2) throws Exception {
                        String output = typeRank + ":\n";
                        output += "ts: " + CalendarUtils.getTimestampFormatted(new Date(longArrayListTuple2.f0)) + "\n";

                        for (int i = 0; i < longArrayListTuple2.f1.size(); i++) {
                            output += id + ": " + i + " " + value + ": " + longArrayListTuple2.f1.get(i) + "\n";
                        }
                        return output;
                    }
                })
                .addSink(producer);
    }
}
