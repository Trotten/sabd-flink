package Utils;


import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;

import java.util.ArrayList;

/**
 * Estensione dell'oggetto ArrayList.
 */

public class OrderedArrayList {

    public static class ArrayListOrderedIncreasingIndex0 extends ArrayList<Tuple2<Integer, Long>> {

        /**
         * Metodo che in fase di inserimento posizione il task da inserire in una posizione ordinata in base al tempo.
         *
         * @param tuple2, task da inserire nella lista di eventi.
         */
        public void insertOrdered(Tuple2<Integer, Long> tuple2) {
            // loop through all elements
            for (int i = 0; i < size(); i++) {

                // if the element you are looking at is smaller than x,
                // go to the next element
                if (get(i).f0 < tuple2.f0)
                    continue;
                // otherwise, we have found the location to add x
                add(i, tuple2);
                return;
            }
            // we looked through all of the elements, and they were all
            // smaller than x, so we add ax to the end of the list
            add(tuple2);
        }

    }

    public static class ArrayListOrderedIncreasingIndex1 extends ArrayList<Tuple2<Long, Long>> {

        /**
         * Metodo che in fase di inserimento posizione il task da inserire in una posizione ordinata in base al tempo.
         *
         * @param tuple2, task da inserire nella lista di eventi.
         */
        public void insertOrdered(Tuple2<Long, Long> tuple2) {
            // loop through all elements
            for (int i = 0; i < size(); i++) {

                // if the element you are looking at is smaller than x,
                // go to the next element
                if (get(i).f1 < tuple2.f1)
                    continue;
                // otherwise, we have found the location to add x
                add(i, tuple2);
                return;
            }
            // we looked through all of the elements, and they were all
            // smaller than x, so we add ax to the end of the list
            add(tuple2);
        }

    }

    public static class ArrayListOrderedDecreasingIndex2 extends ArrayList<Tuple3<Long, Long, Long>> {

        /**
         * Metodo che in fase di inserimento posizione il task da inserire in una posizione ordinata in base al tempo.
         *
         * @param tuple3, task da inserire nella lista di eventi.
         */
        public void insertOrdered(Tuple3<Long, Long, Long> tuple3) {
            // loop through all elements
            for (int i = 0; i < size(); i++) {

                // if the element you are looking at is smaller than x,
                // go to the next element
                if (get(i).f2 > tuple3.f2)
                    continue;
                // otherwise, we have found the location to add x
                add(i, tuple3);
                return;
            }
            // we looked through all of the elements, and they were all
            // smaller than x, so we add ax to the end of the list
            add(tuple3);
        }

    }


    public static class ArrayListLongOrderedDecreasing extends ArrayList<Long> {

        /**
         * Metodo che in fase di inserimento posizione il task da inserire in una posizione ordinata in base al tempo.
         *
         * @param myLong, task da inserire nella lista di eventi.
         */
        public void insertOrdered(Long myLong) {
            // loop through all elements
            for (int i = 0; i < size(); i++) {

                // if the element you are looking at is smaller than x,
                // go to the next element
                if (get(i) > myLong)
                    continue;
                // otherwise, we have found the location to add x
                add(i, myLong);
                return;
            }
            // we looked through all of the elements, and they were all
            // smaller than x, so we add ax to the end of the list
            add(myLong);
        }

    }


}
