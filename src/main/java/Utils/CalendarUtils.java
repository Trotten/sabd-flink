package Utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarUtils {

    public static Integer getHourOfDay(Date date) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static String getTimestampFormatted(Date date) {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(date);
        return "Anno: " + calendar.get(Calendar.YEAR) + " Mese: " + String.valueOf(calendar.get(Calendar.MONTH) + 1) + " Giorno: " + calendar.get(Calendar.DAY_OF_MONTH) + " Ora:" + calendar.get(Calendar.HOUR_OF_DAY) + " Minuti: " + calendar.get(Calendar.MINUTE) + " Secondi: " + calendar.get(Calendar.SECOND);
    }
}
