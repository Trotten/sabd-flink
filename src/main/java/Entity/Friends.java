package Entity;

import java.util.Date;

public class Friends {
    private Date ts;
    private Long user_id_1;
    private Long user_id_2;

    public Friends(Date ts, Long user_id_1, Long user_id_2) {
        this.ts = ts;
        this.user_id_1 = user_id_1;
        this.user_id_2 = user_id_2;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public Long getUser_id_1() {
        return user_id_1;
    }

    public void setUser_id_1(Long user_id_1) {
        this.user_id_1 = user_id_1;
    }

    public Long getUser_id_2() {
        return user_id_2;
    }

    public void setUser_id_2(Long user_id_2) {
        this.user_id_2 = user_id_2;
    }
}
