package Entity;

import java.util.Date;

public class Posts {

    private Date ts;
    private Long post_id;
    private Long user_id;
    private String post;
    private String user;

    public Posts(Date ts, Long post_id, Long user_id, String post, String user) {
        this.ts = ts;
        this.post_id = post_id;
        this.user_id = user_id;
        this.post = post;
        this.user = user;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public Long getPost_id() {
        return post_id;
    }

    public void setPost_id(Long post_id) {
        this.post_id = post_id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
