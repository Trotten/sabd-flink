package Main;

public class GLOBAL {

    public static String ZOOKEEPER = "35.193.244.179:2181";
    public static String BROKER = "35.225.218.158:9092";
    /*public static  String  ZOOKEEPER ="52.18.180.9:2181";
    public static  String  BROKER ="34.243.170.15:9092";*/

    public static String TOPIC_CONSUMER_STREAM_FRIENDS = "Friends";
    public static String TOPIC_CONSUMER_STREAM_POSTS = "Posts";
    public static String TOPIC_CONSUMER_STREAM_COMMENTS = "Comments";

    public static String TOPIC_PRODUCER_QUERY1 = "Query1";
    public static String TOPIC_PRODUCER_QUERY2 = "Query2";
    public static String TOPIC_PRODUCER_QUERY3 = "Query3";


}
