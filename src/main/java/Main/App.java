package Main;

import Filtraggio.FiltroCommentiDiretti;
import Filtraggio.FiltroFriends;
import Filtraggio.FiltroPosts;
import QueryFlink.PrimaQuery;
import QueryFlink.SecondaQuery;
import QueryFlink.TerzaQuery;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer010;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer010;

import java.util.Properties;

public class App {

    public static void main(String[] args) throws Exception {


        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);


        //consumer kafka
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", GLOBAL.BROKER);
        properties.setProperty("zookeeper.connect", GLOBAL.ZOOKEEPER);
        //properties.setProperty("groupid","monitoring");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        FlinkKafkaConsumer010<String> streamFriendship = new FlinkKafkaConsumer010<String>(GLOBAL.TOPIC_CONSUMER_STREAM_FRIENDS, new SimpleStringSchema(), properties);
        FlinkKafkaConsumer010<String> streamPosts = new FlinkKafkaConsumer010<String>(GLOBAL.TOPIC_CONSUMER_STREAM_POSTS, new SimpleStringSchema(), properties);
        FlinkKafkaConsumer010<String> streamComments = new FlinkKafkaConsumer010<String>(GLOBAL.TOPIC_CONSUMER_STREAM_COMMENTS, new SimpleStringSchema(), properties);


        FlinkKafkaProducer010<String> producerQuery1 = new FlinkKafkaProducer010<String>(
                GLOBAL.BROKER, GLOBAL.TOPIC_PRODUCER_QUERY1, new SimpleStringSchema());
        producerQuery1.setLogFailuresOnly(false);
        producerQuery1.setFlushOnCheckpoint(true);

        FlinkKafkaProducer010<String> producerQuery2 = new FlinkKafkaProducer010<String>(
                GLOBAL.BROKER, GLOBAL.TOPIC_PRODUCER_QUERY2, new SimpleStringSchema());
        producerQuery2.setLogFailuresOnly(false);
        producerQuery2.setFlushOnCheckpoint(true);


        FlinkKafkaProducer010<String> producerQuery3 = new FlinkKafkaProducer010<String>(
                GLOBAL.BROKER, GLOBAL.TOPIC_PRODUCER_QUERY3, new SimpleStringSchema());
        producerQuery3.setLogFailuresOnly(false);
        producerQuery3.setFlushOnCheckpoint(true);

        //dataset prima query

        DataStream<Tuple3<Long, Long, Long>> streamFriendParsed = FiltroFriends.executeFilter(env.addSource(streamFriendship));

        //dataset seconda query
        DataStream<Tuple2<Long, Long>> streamCommentsQuery2 = FiltroCommentiDiretti.executeFilterCommentsID(env.addSource(streamComments));

        //dataset terza query
        DataStream<Tuple2<Long, Long>> streamPostsQuery3 = FiltroPosts.executeFilterPostsUserID(env.addSource(streamPosts));
        DataStream<Tuple2<Long, Long>> streamCommentsQuery3 = FiltroCommentiDiretti.executeUserIDComments(env.addSource(streamComments));


        streamFriendParsed = streamFriendParsed.assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Tuple3<Long, Long, Long>>() {
            @Override
            public long extractAscendingTimestamp(Tuple3<Long, Long, Long> longLongLongTuple3) {
                return longLongLongTuple3.f0;
            }
        });
        streamCommentsQuery2.assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Tuple2<Long, Long>>() {
            @Override
            public long extractAscendingTimestamp(Tuple2<Long, Long> longLongTuple2) {
                return longLongTuple2.f0;
            }
        });
        streamPostsQuery3.assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Tuple2<Long, Long>>() {
            @Override
            public long extractAscendingTimestamp(Tuple2<Long, Long> longLongTuple2) {
                return longLongTuple2.f0;
            }
        });
        streamCommentsQuery3.assignTimestampsAndWatermarks(new AscendingTimestampExtractor<Tuple2<Long, Long>>() {
            @Override
            public long extractAscendingTimestamp(Tuple2<Long, Long> longLongTuple2) {
                return longLongTuple2.f0;
            }
        });

        PrimaQuery.executeQuery1(streamFriendParsed, producerQuery1);

        SecondaQuery.executeSecondQuery(streamCommentsQuery2, producerQuery2);

        TerzaQuery.executeThirdQuery(streamFriendParsed, streamPostsQuery3, streamCommentsQuery3, producerQuery3);

        env.execute();

    }
}
