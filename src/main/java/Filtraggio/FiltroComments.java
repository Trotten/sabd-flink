package Filtraggio;

import Entity.Comments;
import Utils.StringUtils;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.streaming.api.datastream.DataStream;

import java.util.Date;

public class FiltroComments {

    public static DataStream<Comments> executeFilterComments(DataStream<String> stream) {
        return stream
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        return SocialREGEX.filterComment(s);
                    }
                })
                .map(new MapFunction<String, Comments>() {
                    @Override
                    public Comments map(String s) throws Exception {
                        String[] strings = s.split(SocialREGEX.R_SEPARATOR);
                        Date date = StringUtils.getDateFromString(strings[0]);
                        return new Comments(date, Long.valueOf(strings[1]), Long.valueOf(strings[2]), strings[3], strings[4], strings[5].compareTo("") == 0 ? null : Long.valueOf(strings[5]), strings[6].compareTo("") == 0 ? null : Long.valueOf(strings[6]));
                    }
                });
    }
}
