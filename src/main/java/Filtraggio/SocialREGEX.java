package Filtraggio;

public class SocialREGEX {

    //-------- Di seguito vengono commentate tutte le espressioni regolari utilizzate --------
    //Ogni espressione regolare viene anticipata da un \ e nel caso di Java \\


    public static String R_TIMESTAMP = "\\d{4}-[0-1]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d\\.\\d{3}\\+\\d{4}";
    /*
        Espressione regolare che controlla il formato del timestamp (Esempio : 2010-02-09T04:05:20.777+0000)
        \\d{4} si aspetta un intero decimale di 4 elementi per l'anno ( 2010 )

        seguito da un separatore ( - )

        [0-1] un intero tra 0 ed 1 ( 0 ) per il mese
        \\d un intero tra 0 e 9

        seguito da un separatore ( - )

        [0-3] un intero tra 0 ed 3 ( 0 ) per il giorno
        \\d un intero tra 0 e 9

        seguito dal carattere T

        [0-2] per rappresentare le il primo valore dell'ora
        \\d un intero tra 0 e 9

        seguito dal separatore : per ore:minuti:secondi

        [0-5] per rappresentare le il primo valore dei minuti
        \\d un intero tra 0 e 9

        seguito dal separatore : per ore:minuti:secondi

        [0-5] per rappresentare le il primo valore dei secondi
        \\d un intero tra 0 e 9

        seguito dal carattere .

        \\d{3} si aspetta un intero decimale di 3 elementi per i millisecondi ( 777 )

        seguito dal carattere +

        \\d{4} si aspetta un intero decimale di 4 elementi per il fuso orario ( 0000 )    */

    public static String R_ID = "\\d+";
    /*
        Espressione regolare che controlla gli id
        \\d+ si aspetta un intero decimale di qualsiasi grandezza per gli id    */

    public static String R_TEXT = "\\S+";
    /*
        Espressione regolare che controlla i testi
        \\S+ si aspetta una stringa di qualsiasi grandezza     */

    public static String R_TEXT_OR_NULL = ".*(\\S+|)";
    /*
        Espressione regolare usata per i campi testo o nulli
        .*(\S+|) mette in OR logico una stringa di qualsiasi grandezza o un campo nullo     */

    public static String R_SEPARATOR = "\\|";
    /*
        Espressione regolare utilizzata per il separatore
        \\| si aspetta il separatore | tra un'espressione e l'altra    */

    public static String R_POST_OR_NULL = "\\.*(\\w+(\\.\\w+)+|)";
    /*
        Espressione regolare che verifica se un post è presente (46327469219.jpg) o nullo
        A differenza di R_TEXT_OR_NULL garantisce anche la presenza del carattere . ( 46327469219.jpg )
        \\w+ si aspetta un qualsiasi carattere alfanumerico     */


    public static String REGEX_FRIENDSHIP = R_TIMESTAMP
            + R_SEPARATOR
            + R_ID
            + R_SEPARATOR
            + R_ID;

    public static String REGEX_POST = R_TIMESTAMP
            + R_SEPARATOR
            + R_ID //post_id
            + R_SEPARATOR
            + R_ID //user_id
            + R_SEPARATOR
            + R_POST_OR_NULL //post
            + R_SEPARATOR
            + R_TEXT; //user

    public static String REGEX_COMMENT = R_TIMESTAMP
            + R_SEPARATOR
            + R_ID //comment_id
            + R_SEPARATOR
            + R_ID //user_id
            + R_SEPARATOR
            + R_TEXT //comment
            + R_SEPARATOR
            + R_TEXT //user
            + R_SEPARATOR
            + R_TEXT_OR_NULL //comment_repl (Exist or not)
            + R_SEPARATOR
            + R_TEXT_OR_NULL; //post_repl (Exist or not)

    public static String REGEX_COMMENT_ONLY_DIRECT_COMMENT = R_TIMESTAMP
            + R_SEPARATOR
            + R_ID //comment_id
            + R_SEPARATOR
            + R_ID //user_id
            + R_SEPARATOR
            + R_TEXT //comment
            + R_SEPARATOR
            + R_TEXT //user
            + R_SEPARATOR //comment_repl (Exist or not)
            + R_SEPARATOR
            + R_TEXT_OR_NULL; //post_repl (Exist or not)


    public static boolean filterFriendship(String line) {
        return line.replaceAll("\\s+", "").matches(REGEX_FRIENDSHIP);
    }

    public static boolean filterPost(String line) {
        return line.replaceAll("\\s+", "").matches(REGEX_POST);
    }

    public static boolean filterComment(String line) {
        return line.replaceAll("\\s+", "").matches(REGEX_COMMENT);
    }

    public static boolean filterOnlyDirectComment(String line) {
        return line.replaceAll("\\s+", "").matches(REGEX_COMMENT_ONLY_DIRECT_COMMENT);
    }


}
