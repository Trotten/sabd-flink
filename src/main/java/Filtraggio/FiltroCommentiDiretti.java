package Filtraggio;

import Utils.StringUtils;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;

import java.text.ParseException;

public class FiltroCommentiDiretti {

    public static DataStream<Tuple2<Long, Long>> executeFilterCommentsID(DataStream<String> stream) {
        return stream
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        return SocialREGEX.filterOnlyDirectComment(s);
                    }
                })
                .assignTimestampsAndWatermarks(new AscendingTimestampExtractor<String>() {
                    @Override
                    public long extractAscendingTimestamp(String s) {
                        String[] strings = s.split(SocialREGEX.R_SEPARATOR);
                        try {
                            return StringUtils.getDateFromString(strings[0]).getTime();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        //non ci arriverà mai perchè il formato è stato già filtrato
                        return 0;
                    }
                })
                .map(new MapFunction<String, Tuple2<Long, Long>>() {
                    @Override
                    public Tuple2<Long, Long> map(String s) throws Exception {
                        String[] strings = s.split(SocialREGEX.R_SEPARATOR);
                        return new Tuple2<>(StringUtils.getDateFromString(strings[0]).getTime(), Long.valueOf(strings[6]));
                    }
                });
    }

    public static DataStream<Tuple2<Long, Long>> executeUserIDComments(DataStream<String> stream) {
        return stream
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        return SocialREGEX.filterComment(s);
                    }
                })
                .map(new MapFunction<String, Tuple2<Long, Long>>() {
                    @Override
                    public Tuple2<Long, Long> map(String s) throws Exception {
                        String[] strings = s.split(SocialREGEX.R_SEPARATOR);
                        return new Tuple2<>(StringUtils.getDateFromString(strings[0]).getTime(), Long.valueOf(strings[2]));
                    }
                });
    }
}
