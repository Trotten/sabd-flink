package Filtraggio;

import Utils.StringUtils;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;

import java.util.Date;

public class FiltroFriends {

    public static DataStream<Tuple3<Long, Long, Long>> executeFilter(DataStream<String> stream) {
        return stream
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        return SocialREGEX.filterFriendship(s);
                    }
                })
                .map(new MapFunction<String, Tuple3<Long, Long, Long>>() {
                    @Override
                    public Tuple3<Long, Long, Long> map(String s) throws Exception {
                        String[] strings = s.split(SocialREGEX.R_SEPARATOR);
                        Date date = StringUtils.getDateFromString(strings[0]);
                        Long user_id_1 = Long.valueOf(strings[1]);
                        Long user_id_2 = Long.valueOf(strings[2]);
                        return new Tuple3<>(date.getTime(), user_id_1, user_id_2);

                    }
                });

    }

    public static DataStream<Long> filterDateFriendship(DataStream<String> stream) {
        return stream
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        return SocialREGEX.filterFriendship(s);
                    }
                })
                .map(new MapFunction<String, Long>() {
                    @Override
                    public Long map(String s) throws Exception {
                        String[] strings = s.split(SocialREGEX.R_SEPARATOR);
                        return StringUtils.getDateFromString(strings[0]).getTime();
                    }
                });

    }
}
