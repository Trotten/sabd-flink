package Filtraggio;

import Entity.Posts;
import Utils.StringUtils;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.streaming.api.datastream.DataStream;

import java.util.Date;

public class FiltroPosts {

    public static DataStream<Posts> executeFilterPosts(DataStream<String> stream) {
        return stream
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        return SocialREGEX.filterPost(s);
                    }
                })
                .map(new MapFunction<String, Posts>() {
                    @Override
                    public Posts map(String s) throws Exception {
                        String[] strings = s.split(SocialREGEX.R_SEPARATOR);
                        Date date = StringUtils.getDateFromString(strings[0]);
                        return new Posts(date, Long.valueOf(strings[1]), Long.valueOf(strings[2]), strings[3], strings[4]);
                    }
                });
    }

    public static DataStream<Tuple2<Long, Long>> executeFilterPostsUserID(DataStream<String> stream) {
        return stream
                .filter(new FilterFunction<String>() {
                    @Override
                    public boolean filter(String s) throws Exception {
                        return SocialREGEX.filterPost(s);
                    }
                })
                .map(new MapFunction<String, Tuple2<Long, Long>>() {
                    @Override
                    public Tuple2<Long, Long> map(String s) throws Exception {
                        String[] strings = s.split(SocialREGEX.R_SEPARATOR);
                        return new Tuple2<>(StringUtils.getDateFromString(strings[0]).getTime(), Long.valueOf(strings[2]));
                    }
                });
    }
}
